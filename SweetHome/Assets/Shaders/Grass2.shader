// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Environment/Grass2"
{
	Properties
	{
		Player_Position("Player_Position", Vector) = (0,0,0,0)
		Home_Position("Home_Position", Vector) = (0,0,0,0)
		_SphereEffectRadius("SphereEffectRadius", Range( 0 , 10)) = 0
		_PlayerEffectRadius("PlayerEffectRadius", Range( 0 , 10)) = 0
		_EffectClampMax("EffectClampMax", Range( 0 , 10)) = 0
		_GravityGradientStrenght("GravityGradientStrenght", Range( 0 , 1)) = 0.7
		_Float2("Float 2", Range( 0 , 1)) = 0.7
		_GravityFixedRoots("GravityFixedRoots", Range( 0 , 1)) = 1
		_Float4("Float 4", Range( 0 , 1)) = 1
		_GravityMultiplier("GravityMultiplier", Range( 0 , 10)) = 2
		_OffsetMultiplier("OffsetMultiplier", Range( 0 , 10)) = 2
		_AlphaMask("AlphaMask", 2D) = "white" {}
		_colorTip("colorTip", Color) = (0.2835906,0.9056604,0,0)
		_colorRoot("colorRoot", Color) = (0.4433962,0.2028133,0,0)
		_AlbedoTexture("AlbedoTexture", 2D) = "white" {}
		_Metallic("Metallic", Range( 0 , 1)) = 0
		_EffectTopOffset("EffectTopOffset", Float) = 2
		_smoothness("smoothness", Range( 0 , 1)) = 0
		_EffectBottomOffset("EffectBottomOffset", Float) = -1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Transparent+0" }
		Cull Off
		AlphaToMask On
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
		};

		uniform float _Float2;
		uniform float _Float4;
		uniform float _SphereEffectRadius;
		uniform float3 Player_Position;
		uniform float3 Home_Position;
		uniform float _EffectTopOffset;
		uniform float _EffectBottomOffset;
		uniform float _EffectClampMax;
		uniform float _OffsetMultiplier;
		uniform float _GravityMultiplier;
		uniform float _GravityGradientStrenght;
		uniform float _GravityFixedRoots;
		uniform float _PlayerEffectRadius;
		uniform sampler2D _AlbedoTexture;
		uniform float4 _AlbedoTexture_ST;
		uniform float4 _colorTip;
		uniform float4 _colorRoot;
		uniform float _Metallic;
		uniform float _smoothness;
		uniform sampler2D _AlphaMask;
		uniform float4 _AlphaMask_ST;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float lerpResult50 = lerp( v.texcoord.xy.y , 1.0 , ( 1.0 - _Float2 ));
			float blendOpSrc57 = v.texcoord.xy.y;
			float blendOpDest57 = lerpResult50;
			float lerpResult56 = lerp( ( saturate( ( blendOpSrc57 * blendOpDest57 ) )) , 1.0 , ( 1.0 - _Float4 ));
			float ZXOffset58 = lerpResult56;
			float3 ase_vertex3Pos = v.vertex.xyz;
			float3 break3 = Player_Position;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float WorldY79 = ase_worldPos.y;
			float3 break95 = Home_Position;
			float HomeY96 = break95.y;
			float HomeTopY149 = ( HomeY96 + _EffectTopOffset );
			float HomeBottomY146 = ( HomeY96 + _EffectBottomOffset );
			float VertexY_Home98 = ase_vertex3Pos.y;
			float RecalHomeY155 = (( WorldY79 > HomeTopY149 ) ? HomeTopY149 :  (( WorldY79 < HomeBottomY146 ) ? HomeBottomY146 :  VertexY_Home98 ) );
			float3 appendResult7 = (float3(break3.x , RecalHomeY155 , break3.z));
			float clampResult12 = clamp( ( _SphereEffectRadius - distance( ase_vertex3Pos , appendResult7 ) ) , 0.0 , _EffectClampMax );
			float3 break14 = Player_Position;
			float3 appendResult18 = (float3(break14.x , ase_vertex3Pos.y , break14.z));
			float3 normalizeResult17 = normalize( ( ase_vertex3Pos - appendResult18 ) );
			float ZXMultiplier44 = _OffsetMultiplier;
			float3 _Gravity = float3(0,-1,0);
			float3 normalizeResult37 = normalize( -_Gravity );
			float3 YMultiplier40 = ( normalizeResult37 * _GravityMultiplier );
			float lerpResult25 = lerp( v.texcoord.xy.y , 1.0 , ( 1.0 - _GravityGradientStrenght ));
			float blendOpSrc28 = v.texcoord.xy.y;
			float blendOpDest28 = lerpResult25;
			float lerpResult29 = lerp( ( saturate( ( blendOpSrc28 * blendOpDest28 ) )) , 1.0 , ( 1.0 - _GravityFixedRoots ));
			float YOffset33 = lerpResult29;
			float3 appendResult103 = (float3(break95.x , RecalHomeY155 , break95.z));
			float clampResult129 = clamp( ( _PlayerEffectRadius - distance( ase_vertex3Pos , appendResult103 ) ) , 0.0 , _EffectClampMax );
			float3 break109 = Home_Position;
			float3 appendResult116 = (float3(break109.x , ase_vertex3Pos.y , break109.z));
			float3 normalizeResult126 = normalize( ( ase_vertex3Pos - appendResult116 ) );
			float3 normalizeResult118 = normalize( -_Gravity );
			float3 YHmultiplier132 = ( normalizeResult118 * _GravityMultiplier );
			float lerpResult112 = lerp( v.texcoord.xy.y , 1.0 , ( 1.0 - _GravityGradientStrenght ));
			float blendOpSrc120 = v.texcoord.xy.y;
			float blendOpDest120 = lerpResult112;
			float lerpResult128 = lerp( ( saturate( ( blendOpSrc120 * blendOpDest120 ) )) , 1.0 , ( 1.0 - _GravityFixedRoots ));
			float YHoffset130 = lerpResult128;
			v.vertex.xyz += ( ( ( ZXOffset58 * ( clampResult12 * normalizeResult17 ) * ZXMultiplier44 ) + ( YMultiplier40 * YOffset33 * -clampResult12 ) ) + ( ( ZXOffset58 * ( clampResult129 * normalizeResult126 ) * ZXMultiplier44 ) + ( YHmultiplier132 * YHoffset130 * -clampResult129 ) ) );
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_AlbedoTexture = i.uv_texcoord * _AlbedoTexture_ST.xy + _AlbedoTexture_ST.zw;
			float4 lerpResult67 = lerp( _colorTip , _colorRoot , ( 1.0 - i.uv_texcoord.y ));
			float4 blendOpSrc69 = tex2D( _AlbedoTexture, uv_AlbedoTexture );
			float4 blendOpDest69 = lerpResult67;
			o.Albedo = ( saturate( ( blendOpSrc69 * blendOpDest69 ) )).rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _smoothness;
			float2 uv_AlphaMask = i.uv_texcoord * _AlphaMask_ST.xy + _AlphaMask_ST.zw;
			o.Alpha = tex2D( _AlphaMask, uv_AlphaMask ).r;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			AlphaToMask Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16200
7;1;1352;698;3597.54;-261.6803;2.935488;True;False
Node;AmplifyShaderEditor.Vector3Node;139;-3012.031,2007.432;Float;False;Property;Home_Position;Home_Position;1;0;Create;False;0;0;False;0;0,0,0;0.53,0,-1.666;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.BreakToComponentsNode;95;-2524.984,1925.559;Float;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RegisterLocalVarNode;96;-2262.079,1919.033;Float;False;HomeY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;141;-2595.74,-2660.91;Float;False;96;HomeY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;76;-2704,-1712;Float;False;Property;_EffectBottomOffset;EffectBottomOffset;19;0;Create;True;0;0;False;0;-1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;144;-2259.74,-2756.91;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;78;-2642.846,-1311.711;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;143;-2595.74,-2436.91;Float;False;96;HomeY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;77;-2704,-1456;Float;False;Property;_EffectTopOffset;EffectTopOffset;17;0;Create;True;0;0;False;0;2;6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;97;-2472.188,1740.773;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;145;-2243.74,-2484.91;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;98;-2272.281,1726.818;Float;False;VertexY_Home;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;79;-2234.879,-1295.059;Float;False;WorldY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;146;-2099.74,-2772.91;Float;False;HomeBottomY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;149;-2035.739,-2500.91;Float;False;HomeTopY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;147;-1290.266,-2130.869;Float;False;98;VertexY_Home;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;148;-1363.738,-2356.91;Float;False;146;HomeBottomY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;150;-1347.738,-2452.91;Float;False;79;WorldY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;151;-1011.739,-2436.91;Float;False;149;HomeTopY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;153;-1027.739,-2532.91;Float;False;79;WorldY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareLower;152;-995.7388,-2276.91;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareGreater;154;-627.7394,-2468.91;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;155;-53.1704,-2320.789;Float;False;RecalHomeY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;2;-2872.297,303.7653;Float;False;Property;Player_Position;Player_Position;0;0;Create;False;0;0;False;0;0,0,0;0.772,0.376,-1.002;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.BreakToComponentsNode;3;-2385.25,211.9893;Float;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;21;-2672,1216;Float;False;Property;_GravityGradientStrenght;GravityGradientStrenght;5;0;Create;True;0;0;False;0;0.7;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;47;-2919.846,-552.9138;Float;False;Property;_Float2;Float 2;6;0;Create;True;0;0;False;0;0.7;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;93;-2918.438,-1012.468;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;92;-2040.197,128.7011;Float;False;155;RecalHomeY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;100;-2703.043,2462.726;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;101;-2183.073,1842.966;Float;False;155;RecalHomeY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;94;-2663.894,776.5884;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;49;-2535.846,-424.9136;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;26;-1836.06,944;Float;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexCoordVertexDataNode;51;-2117.414,-803.1691;Float;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;105;-2464.362,2674.634;Float;True;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;23;-2320,1232;Float;False;Constant;_Float0;Float 0;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;7;-1787.817,224.4409;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PosVertexDataNode;15;-2350.689,404.9604;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;20;-2324.628,961.064;Float;True;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;14;-2362.943,564.2548;Float;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.OneMinusNode;24;-2288,1344;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;-2567.846,-536.9136;Float;False;Constant;_Float3;Float 3;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;4;-2332.454,27.20317;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PosVertexDataNode;108;-2490.423,2118.531;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;104;-2427.734,3057.569;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;103;-1927.55,1938.011;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BreakToComponentsNode;109;-2502.677,2277.825;Float;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.TexCoordVertexDataNode;102;-1975.793,2657.57;Float;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;46;-2598.564,-772.8387;Float;True;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;35;-1685.163,767.3688;Float;False;Constant;_Gravity;Gravity;5;0;Create;True;0;0;False;0;0,-1,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DistanceOpNode;8;-1613.245,88.66679;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;111;-1426.162,2456.407;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;54;-1911.846,-408.9137;Float;False;Property;_Float4;Float 4;8;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;25;-2000,1136;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;9;-2100.782,-113.6588;Float;False;Property;_SphereEffectRadius;SphereEffectRadius;2;0;Create;True;0;0;False;0;0;1.76;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-1664,1360;Float;False;Property;_GravityFixedRoots;GravityFixedRoots;7;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;52;-1847.255,-766.1218;Float;False;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;157;-1993.677,1629.975;Float;False;Property;_PlayerEffectRadius;PlayerEffectRadius;3;0;Create;True;0;0;False;0;0;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;50;-2247.846,-632.9134;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;27;-1520,1008;Float;False;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;113;-1659.733,2721.57;Float;False;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;112;-2139.734,2849.57;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;115;-1752.978,1802.236;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;36;-1286.429,742.8373;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;116;-2245.328,2275.374;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;18;-2105.593,561.804;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;16;-1970.835,495.6356;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BlendOpsNode;28;-1472,1088;Float;False;Multiply;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;53;-1623.846,-408.9137;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-1408,1232;Float;False;Constant;_Float1;Float 1;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;57;-1719.846,-680.9135;Float;False;Multiply;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;10;-1444.254,-1.945965;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;117;-2110.569,2209.206;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NormalizeNode;37;-1137.771,746.4193;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;39;-1247.026,853.8832;Float;False;Property;_GravityMultiplier;GravityMultiplier;9;0;Create;True;0;0;False;0;2;4;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;122;-1583.987,1711.624;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;118;-1277.504,2459.989;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;32;-1376,1360;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;120;-1611.733,2801.57;Float;False;Multiply;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;11;-1635.534,321.2891;Float;False;Property;_EffectClampMax;EffectClampMax;4;0;Create;True;0;0;False;0;0;10;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;55;-1655.846,-536.9136;Float;False;Constant;_Float5;Float 5;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;119;-1515.733,3073.569;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;56;-1447.846,-648.9135;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;17;-1806.639,500.5367;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ClampOpNode;12;-1270.669,77.11236;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;29;-1200,1120;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;43;-1395.652,542.2369;Float;False;Property;_OffsetMultiplier;OffsetMultiplier;10;0;Create;True;0;0;False;0;2;1.44;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;126;-1946.372,2214.107;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;128;-1339.733,2833.57;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;129;-1410.402,1790.682;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;127;-1109.144,2463.571;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;-969.4109,750.0016;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NegateNode;41;-987.499,440.1984;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;33;-928,1152;Float;False;YOffset;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;133;-1182.382,1842.544;Float;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;40;-813.5886,776.8674;Float;False;YMultiplier;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NegateNode;134;-1127.232,2153.769;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;44;-1091.967,539.8073;Float;False;ZXMultiplier;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;64;-735.4495,-281.054;Float;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;130;-1067.733,2867.063;Float;False;YHoffset;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;58;-1175.846,-616.9135;Float;False;ZXOffset;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-1042.649,128.9744;Float;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;132;-953.3215,2490.437;Float;False;YHmultiplier;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;136;-714.5569,2563.878;Float;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;135;-767.6255,2196.254;Float;False;3;3;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-627.8931,482.6841;Float;False;3;3;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;63;-769.1744,-522.7484;Float;False;Property;_colorRoot;colorRoot;14;0;Create;True;0;0;False;0;0.4433962,0.2028133,0,0;0.245283,0.1128736,0,0;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;65;-521.8589,-230.4668;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-574.8246,850.3083;Float;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;62;-774.7951,-711.0452;Float;False;Property;_colorTip;colorTip;13;0;Create;True;0;0;False;0;0.2835906,0.9056604,0,0;0.3116884,1,0,0;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;60;-412.2532,576.118;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;68;-460.0302,-767.2533;Float;True;Property;_AlbedoTexture;AlbedoTexture;15;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;67;-412.2533,-542.4213;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;138;-551.9857,2289.688;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;85;-928,-1728;Float;False;79;WorldY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;84;-1248,-1424;Float;False;5;VertexY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;61;-572.4458,281.026;Float;True;Property;_AlphaMask;AlphaMask;12;0;Create;True;0;0;False;0;7a170cdb7cc88024cb628cfcdbb6705c;d451ad33ff76f4e409d4a6d2819bf498;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;70;-445.978,-14.06598;Float;False;Property;_Metallic;Metallic;16;0;Create;True;0;0;False;0;0;0.74;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;75;-2144,-1680;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareGreater;89;-528,-1664;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;6;-2108.22,186.6303;Float;False;PlayerY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;82;-1248,-1648;Float;False;79;WorldY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;86;-912,-1632;Float;False;80;PlayerTopY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;80;-1936,-1696;Float;False;PlayerTopY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;91;-48,-1536;Float;False;RecalculatedY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;81;-2000,-1968;Float;False;PlayerBottomY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;156;-201.026,596.4338;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;74;-2160,-1952;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareLower;87;-896,-1472;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;71;-423.4948,92.72926;Float;False;Property;_smoothness;smoothness;18;0;Create;True;0;0;False;0;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;69;-114.3508,-503.0757;Float;True;Multiply;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;5;-2132.546,13.24856;Float;False;VertexY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;73;-2496,-1632;Float;False;6;PlayerY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;45;-2910.748,-780.0154;Float;True;Property;_OffsetGradient;OffsetGradient;11;0;Create;True;0;0;False;0;2db959ac191899744984f3441aa74c3c;2db959ac191899744984f3441aa74c3c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;72;-2496,-1856;Float;False;6;PlayerY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;131;-1230.066,2253.377;Float;False;ZXHmultiplier;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;83;-1264,-1552;Float;False;81;PlayerBottomY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Environment/Grass2;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Translucent;0.5;True;True;0;False;Opaque;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;True;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;95;0;139;0
WireConnection;96;0;95;1
WireConnection;144;0;141;0
WireConnection;144;1;76;0
WireConnection;145;0;143;0
WireConnection;145;1;77;0
WireConnection;98;0;97;2
WireConnection;79;0;78;2
WireConnection;146;0;144;0
WireConnection;149;0;145;0
WireConnection;152;0;150;0
WireConnection;152;1;148;0
WireConnection;152;2;148;0
WireConnection;152;3;147;0
WireConnection;154;0;153;0
WireConnection;154;1;151;0
WireConnection;154;2;151;0
WireConnection;154;3;152;0
WireConnection;155;0;154;0
WireConnection;3;0;2;0
WireConnection;49;0;47;0
WireConnection;105;0;100;2
WireConnection;7;0;3;0
WireConnection;7;1;92;0
WireConnection;7;2;3;2
WireConnection;20;0;94;2
WireConnection;14;0;2;0
WireConnection;24;0;21;0
WireConnection;104;0;21;0
WireConnection;103;0;95;0
WireConnection;103;1;101;0
WireConnection;103;2;95;2
WireConnection;109;0;139;0
WireConnection;46;0;93;2
WireConnection;8;0;4;0
WireConnection;8;1;7;0
WireConnection;111;0;35;0
WireConnection;25;0;20;0
WireConnection;25;1;23;0
WireConnection;25;2;24;0
WireConnection;52;0;51;2
WireConnection;50;0;46;0
WireConnection;50;1;48;0
WireConnection;50;2;49;0
WireConnection;27;0;26;2
WireConnection;113;0;102;2
WireConnection;112;0;105;0
WireConnection;112;1;23;0
WireConnection;112;2;104;0
WireConnection;115;0;97;0
WireConnection;115;1;103;0
WireConnection;36;0;35;0
WireConnection;116;0;109;0
WireConnection;116;1;108;2
WireConnection;116;2;109;2
WireConnection;18;0;14;0
WireConnection;18;1;15;2
WireConnection;18;2;14;2
WireConnection;16;0;15;0
WireConnection;16;1;18;0
WireConnection;28;0;27;0
WireConnection;28;1;25;0
WireConnection;53;0;54;0
WireConnection;57;0;52;0
WireConnection;57;1;50;0
WireConnection;10;0;9;0
WireConnection;10;1;8;0
WireConnection;117;0;108;0
WireConnection;117;1;116;0
WireConnection;37;0;36;0
WireConnection;122;0;157;0
WireConnection;122;1;115;0
WireConnection;118;0;111;0
WireConnection;32;0;31;0
WireConnection;120;0;113;0
WireConnection;120;1;112;0
WireConnection;119;0;31;0
WireConnection;56;0;57;0
WireConnection;56;1;55;0
WireConnection;56;2;53;0
WireConnection;17;0;16;0
WireConnection;12;0;10;0
WireConnection;12;2;11;0
WireConnection;29;0;28;0
WireConnection;29;1;30;0
WireConnection;29;2;32;0
WireConnection;126;0;117;0
WireConnection;128;0;120;0
WireConnection;128;1;30;0
WireConnection;128;2;119;0
WireConnection;129;0;122;0
WireConnection;129;2;11;0
WireConnection;127;0;118;0
WireConnection;127;1;39;0
WireConnection;38;0;37;0
WireConnection;38;1;39;0
WireConnection;41;0;12;0
WireConnection;33;0;29;0
WireConnection;133;0;129;0
WireConnection;133;1;126;0
WireConnection;40;0;38;0
WireConnection;134;0;129;0
WireConnection;44;0;43;0
WireConnection;130;0;128;0
WireConnection;58;0;56;0
WireConnection;13;0;12;0
WireConnection;13;1;17;0
WireConnection;132;0;127;0
WireConnection;136;0;132;0
WireConnection;136;1;130;0
WireConnection;136;2;134;0
WireConnection;135;0;58;0
WireConnection;135;1;133;0
WireConnection;135;2;44;0
WireConnection;42;0;58;0
WireConnection;42;1;13;0
WireConnection;42;2;44;0
WireConnection;65;0;64;2
WireConnection;34;0;40;0
WireConnection;34;1;33;0
WireConnection;34;2;41;0
WireConnection;60;0;42;0
WireConnection;60;1;34;0
WireConnection;67;0;62;0
WireConnection;67;1;63;0
WireConnection;67;2;65;0
WireConnection;138;0;135;0
WireConnection;138;1;136;0
WireConnection;75;0;73;0
WireConnection;75;1;77;0
WireConnection;89;0;85;0
WireConnection;89;1;86;0
WireConnection;89;2;86;0
WireConnection;89;3;87;0
WireConnection;6;0;3;1
WireConnection;80;0;75;0
WireConnection;91;0;89;0
WireConnection;81;0;74;0
WireConnection;156;0;60;0
WireConnection;156;1;138;0
WireConnection;74;0;72;0
WireConnection;74;1;76;0
WireConnection;87;0;82;0
WireConnection;87;1;83;0
WireConnection;87;2;83;0
WireConnection;87;3;84;0
WireConnection;69;0;68;0
WireConnection;69;1;67;0
WireConnection;5;0;4;2
WireConnection;131;0;43;0
WireConnection;0;0;69;0
WireConnection;0;3;70;0
WireConnection;0;4;71;0
WireConnection;0;9;61;1
WireConnection;0;11;156;0
ASEEND*/
//CHKSM=4ECF5CD66EA863722690610652D3AC5CE48B0C7B