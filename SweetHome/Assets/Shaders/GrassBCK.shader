// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Environment/GrassBCK"
{
	Properties
	{
		Player_Position("Player_Position", Vector) = (0,0,0,0)
		_EffectRadius("EffectRadius", Range( 0 , 10)) = 0
		_EffectClampMax("EffectClampMax", Range( 0 , 10)) = 0
		_GravityGradientStrenght("GravityGradientStrenght", Range( 0 , 1)) = 0.7
		_Float2("Float 2", Range( 0 , 1)) = 0.7
		_GravityFixedRoots("GravityFixedRoots", Range( 0 , 1)) = 1
		_Float4("Float 4", Range( 0 , 1)) = 1
		_GravityMultiplier("GravityMultiplier", Range( 0 , 10)) = 2
		_OffsetMultiplier("OffsetMultiplier", Range( 0 , 10)) = 2
		_AlphaMask("AlphaMask", 2D) = "white" {}
		_colorTip("colorTip", Color) = (0.2835906,0.9056604,0,0)
		_colorRoot("colorRoot", Color) = (0.4433962,0.2028133,0,0)
		_AlbedoTexture("AlbedoTexture", 2D) = "white" {}
		_Metallic("Metallic", Range( 0 , 1)) = 0
		_EffectTopOffset("EffectTopOffset", Float) = 2
		_smoothness("smoothness", Range( 0 , 1)) = 0
		_EffectBottomOffset("EffectBottomOffset", Float) = -1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Transparent+0" }
		Cull Off
		AlphaToMask On
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
		};

		uniform float _Float2;
		uniform float _Float4;
		uniform float _EffectRadius;
		uniform float3 Player_Position;
		uniform float _EffectTopOffset;
		uniform float _EffectBottomOffset;
		uniform float _EffectClampMax;
		uniform float _OffsetMultiplier;
		uniform float _GravityMultiplier;
		uniform float _GravityGradientStrenght;
		uniform float _GravityFixedRoots;
		uniform sampler2D _AlbedoTexture;
		uniform float4 _AlbedoTexture_ST;
		uniform float4 _colorTip;
		uniform float4 _colorRoot;
		uniform float _Metallic;
		uniform float _smoothness;
		uniform sampler2D _AlphaMask;
		uniform float4 _AlphaMask_ST;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float lerpResult50 = lerp( v.texcoord.xy.y , 1.0 , ( 1.0 - _Float2 ));
			float blendOpSrc57 = v.texcoord.xy.y;
			float blendOpDest57 = lerpResult50;
			float lerpResult56 = lerp( ( saturate( ( blendOpSrc57 * blendOpDest57 ) )) , 1.0 , ( 1.0 - _Float4 ));
			float ZXOffset58 = lerpResult56;
			float3 ase_vertex3Pos = v.vertex.xyz;
			float3 break3 = Player_Position;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float WorldY79 = ase_worldPos.y;
			float PlayerY6 = break3.y;
			float PlayerTopY80 = ( PlayerY6 + _EffectTopOffset );
			float PlayerBottomY81 = ( PlayerY6 + _EffectBottomOffset );
			float VertexY5 = ase_vertex3Pos.y;
			float RecalculatedY91 = (( WorldY79 > PlayerTopY80 ) ? PlayerTopY80 :  (( WorldY79 < PlayerBottomY81 ) ? PlayerBottomY81 :  VertexY5 ) );
			float3 appendResult7 = (float3(break3.x , RecalculatedY91 , break3.z));
			float clampResult12 = clamp( ( _EffectRadius - distance( ase_vertex3Pos , appendResult7 ) ) , 0.0 , _EffectClampMax );
			float3 break14 = Player_Position;
			float3 appendResult18 = (float3(break14.x , ase_vertex3Pos.y , break14.z));
			float3 normalizeResult17 = normalize( ( ase_vertex3Pos - appendResult18 ) );
			float ZXMultiplier44 = _OffsetMultiplier;
			float3 normalizeResult37 = normalize( -float3(0,-1,0) );
			float3 YMultiplier40 = ( normalizeResult37 * _GravityMultiplier );
			float lerpResult25 = lerp( v.texcoord.xy.y , 1.0 , ( 1.0 - _GravityGradientStrenght ));
			float blendOpSrc28 = v.texcoord.xy.y;
			float blendOpDest28 = lerpResult25;
			float lerpResult29 = lerp( ( saturate( ( blendOpSrc28 * blendOpDest28 ) )) , 1.0 , ( 1.0 - _GravityFixedRoots ));
			float YOffset33 = lerpResult29;
			v.vertex.xyz += ( ( ZXOffset58 * ( clampResult12 * normalizeResult17 ) * ZXMultiplier44 ) + ( YMultiplier40 * YOffset33 * -clampResult12 ) );
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_AlbedoTexture = i.uv_texcoord * _AlbedoTexture_ST.xy + _AlbedoTexture_ST.zw;
			float4 lerpResult67 = lerp( _colorTip , _colorRoot , ( 1.0 - i.uv_texcoord.y ));
			float4 blendOpSrc69 = tex2D( _AlbedoTexture, uv_AlbedoTexture );
			float4 blendOpDest69 = lerpResult67;
			o.Albedo = ( saturate( ( blendOpSrc69 * blendOpDest69 ) )).rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _smoothness;
			float2 uv_AlphaMask = i.uv_texcoord * _AlphaMask_ST.xy + _AlphaMask_ST.zw;
			o.Alpha = tex2D( _AlphaMask, uv_AlphaMask ).r;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			AlphaToMask Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16200
472;54;1303;846;3884.477;47.63829;2.424196;True;True
Node;AmplifyShaderEditor.Vector3Node;2;-2806.604,206.9541;Float;False;Property;Player_Position;Player_Position;0;0;Create;False;0;0;False;0;0,0,0;1.566,0,2.211;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.BreakToComponentsNode;3;-2385.25,211.9893;Float;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RegisterLocalVarNode;6;-2108.22,186.6303;Float;False;PlayerY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;72;-2500.697,-1851.34;Float;False;6;PlayerY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;76;-2708.27,-1704.816;Float;False;Property;_EffectBottomOffset;EffectBottomOffset;18;0;Create;True;0;0;False;0;-1;-1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;74;-2159.17,-1952.802;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;78;-2644.846,-1311.711;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;77;-2712.2,-1461.01;Float;False;Property;_EffectTopOffset;EffectTopOffset;16;0;Create;True;0;0;False;0;2;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;73;-2500.53,-1628.095;Float;False;6;PlayerY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;4;-2332.454,27.20317;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;75;-2150.845,-1686.375;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;79;-2236.879,-1295.059;Float;False;WorldY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;81;-2006.529,-1961.128;Float;False;PlayerBottomY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;5;-2132.546,13.24856;Float;False;VertexY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;84;-1259.976,-1417.172;Float;False;5;VertexY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;83;-1265.526,-1544.835;Float;False;81;PlayerBottomY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;82;-1262.752,-1650.296;Float;False;79;WorldY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;80;-1945.472,-1697.476;Float;False;PlayerTopY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareLower;87;-896.4139,-1467.126;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;85;-926.9423,-1725.228;Float;False;79;WorldY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;86;-924.1664,-1636.42;Float;False;80;PlayerTopY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareGreater;89;-530.0757,-1661.397;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;91;-52.72579,-1533.734;Float;False;RecalculatedY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;93;-2918.438,-1012.468;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;92;-2005.023,136.3622;Float;False;91;RecalculatedY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;47;-2919.846,-552.9138;Float;False;Property;_Float2;Float 2;4;0;Create;True;0;0;False;0;0.7;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-2672,1216;Float;False;Property;_GravityGradientStrenght;GravityGradientStrenght;3;0;Create;True;0;0;False;0;0.7;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;94;-2663.894,776.5884;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexCoordVertexDataNode;51;-2117.414,-803.1691;Float;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BreakToComponentsNode;14;-2362.943,564.2548;Float;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.OneMinusNode;49;-2535.846,-424.9136;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;15;-2350.689,404.9604;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;46;-2598.564,-772.8387;Float;True;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;23;-2320,1232;Float;False;Constant;_Float0;Float 0;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;-2567.846,-536.9136;Float;False;Constant;_Float3;Float 3;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;20;-2324.628,961.064;Float;True;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;24;-2288,1344;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;7;-1787.817,224.4409;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;26;-1836.06,944;Float;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector3Node;35;-1476.283,721.3444;Float;False;Constant;_Gravity;Gravity;5;0;Create;True;0;0;False;0;0,-1,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;9;-2100.782,-113.6588;Float;False;Property;_EffectRadius;EffectRadius;1;0;Create;True;0;0;False;0;0;1.7;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;18;-2105.593,561.804;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;52;-1847.255,-766.1218;Float;False;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;8;-1613.245,88.66679;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;27;-1520,1008;Float;False;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;25;-2000,1136;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;54;-1911.846,-408.9137;Float;False;Property;_Float4;Float 4;6;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;50;-2247.846,-632.9134;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;36;-1286.429,742.8373;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-1664,1360;Float;False;Property;_GravityFixedRoots;GravityFixedRoots;5;0;Create;True;0;0;False;0;1;0.7;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;39;-1247.026,853.8832;Float;False;Property;_GravityMultiplier;GravityMultiplier;7;0;Create;True;0;0;False;0;2;2;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;57;-1719.846,-680.9135;Float;False;Multiply;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-1408,1232;Float;False;Constant;_Float1;Float 1;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;10;-1444.254,-1.945965;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;11;-1635.534,321.2891;Float;False;Property;_EffectClampMax;EffectClampMax;2;0;Create;True;0;0;False;0;0;1;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;55;-1655.846,-536.9136;Float;False;Constant;_Float5;Float 5;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;32;-1376,1360;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;37;-1137.771,746.4193;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;53;-1623.846,-408.9137;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;16;-1970.835,495.6356;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BlendOpsNode;28;-1472,1088;Float;False;Multiply;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;-969.4109,750.0016;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;29;-1200,1120;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;56;-1447.846,-648.9135;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;12;-1270.669,77.11236;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;43;-1395.652,542.2369;Float;False;Property;_OffsetMultiplier;OffsetMultiplier;8;0;Create;True;0;0;False;0;2;1;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;64;-735.4495,-281.054;Float;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NormalizeNode;17;-1806.639,500.5367;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NegateNode;41;-987.499,440.1984;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-1042.649,128.9744;Float;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;40;-813.5886,776.8674;Float;False;YMultiplier;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;44;-1091.967,539.8073;Float;False;ZXMultiplier;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;58;-1175.846,-616.9135;Float;False;ZXOffset;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;63;-769.1744,-522.7484;Float;False;Property;_colorRoot;colorRoot;13;0;Create;True;0;0;False;0;0.4433962,0.2028133,0,0;0.245283,0.1128736,0,0;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;62;-774.7951,-711.0452;Float;False;Property;_colorTip;colorTip;12;0;Create;True;0;0;False;0;0.2835906,0.9056604,0,0;0.3116884,1,0,0;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;65;-521.8589,-230.4668;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;33;-928,1152;Float;False;YOffset;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;67;-412.2533,-542.4213;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;68;-460.0302,-767.2533;Float;True;Property;_AlbedoTexture;AlbedoTexture;14;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-627.8931,482.6841;Float;False;3;3;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-574.8246,850.3083;Float;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;70;-445.978,-14.06598;Float;False;Property;_Metallic;Metallic;15;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;45;-2910.748,-780.0154;Float;True;Property;_OffsetGradient;OffsetGradient;10;0;Create;True;0;0;False;0;2db959ac191899744984f3441aa74c3c;2db959ac191899744984f3441aa74c3c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;19;-2672.912,961.0637;Float;True;Property;_GravityGradient;GravityGradient;9;0;Create;True;0;0;False;0;2db959ac191899744984f3441aa74c3c;2db959ac191899744984f3441aa74c3c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendOpsNode;69;-114.3508,-503.0757;Float;True;Multiply;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;71;-423.4948,92.72926;Float;False;Property;_smoothness;smoothness;17;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;61;-572.4458,281.026;Float;True;Property;_AlphaMask;AlphaMask;11;0;Create;True;0;0;False;0;7a170cdb7cc88024cb628cfcdbb6705c;d451ad33ff76f4e409d4a6d2819bf498;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;60;-412.2532,576.118;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Environment/GrassBCK;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Translucent;0.5;True;True;0;False;Opaque;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;True;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;3;0;2;0
WireConnection;6;0;3;1
WireConnection;74;0;72;0
WireConnection;74;1;76;0
WireConnection;75;0;73;0
WireConnection;75;1;77;0
WireConnection;79;0;78;2
WireConnection;81;0;74;0
WireConnection;5;0;4;2
WireConnection;80;0;75;0
WireConnection;87;0;82;0
WireConnection;87;1;83;0
WireConnection;87;2;83;0
WireConnection;87;3;84;0
WireConnection;89;0;85;0
WireConnection;89;1;86;0
WireConnection;89;2;86;0
WireConnection;89;3;87;0
WireConnection;91;0;89;0
WireConnection;14;0;2;0
WireConnection;49;0;47;0
WireConnection;46;0;93;2
WireConnection;20;0;94;2
WireConnection;24;0;21;0
WireConnection;7;0;3;0
WireConnection;7;1;92;0
WireConnection;7;2;3;2
WireConnection;18;0;14;0
WireConnection;18;1;15;2
WireConnection;18;2;14;2
WireConnection;52;0;51;2
WireConnection;8;0;4;0
WireConnection;8;1;7;0
WireConnection;27;0;26;2
WireConnection;25;0;20;0
WireConnection;25;1;23;0
WireConnection;25;2;24;0
WireConnection;50;0;46;0
WireConnection;50;1;48;0
WireConnection;50;2;49;0
WireConnection;36;0;35;0
WireConnection;57;0;52;0
WireConnection;57;1;50;0
WireConnection;10;0;9;0
WireConnection;10;1;8;0
WireConnection;32;0;31;0
WireConnection;37;0;36;0
WireConnection;53;0;54;0
WireConnection;16;0;15;0
WireConnection;16;1;18;0
WireConnection;28;0;27;0
WireConnection;28;1;25;0
WireConnection;38;0;37;0
WireConnection;38;1;39;0
WireConnection;29;0;28;0
WireConnection;29;1;30;0
WireConnection;29;2;32;0
WireConnection;56;0;57;0
WireConnection;56;1;55;0
WireConnection;56;2;53;0
WireConnection;12;0;10;0
WireConnection;12;2;11;0
WireConnection;17;0;16;0
WireConnection;41;0;12;0
WireConnection;13;0;12;0
WireConnection;13;1;17;0
WireConnection;40;0;38;0
WireConnection;44;0;43;0
WireConnection;58;0;56;0
WireConnection;65;0;64;2
WireConnection;33;0;29;0
WireConnection;67;0;62;0
WireConnection;67;1;63;0
WireConnection;67;2;65;0
WireConnection;42;0;58;0
WireConnection;42;1;13;0
WireConnection;42;2;44;0
WireConnection;34;0;40;0
WireConnection;34;1;33;0
WireConnection;34;2;41;0
WireConnection;69;0;68;0
WireConnection;69;1;67;0
WireConnection;60;0;42;0
WireConnection;60;1;34;0
WireConnection;0;0;69;0
WireConnection;0;3;70;0
WireConnection;0;4;71;0
WireConnection;0;9;61;1
WireConnection;0;11;60;0
ASEEND*/
//CHKSM=AD116C0154433B2547C003E29190466838361249