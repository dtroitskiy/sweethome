﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
	public GameObject camera;
	public GameObject beetle;
	public float messageDefaultDelay = 3.0f;
	public float cameraIntroTransitionTime = 1.0f;
	public Vector3 cameraAfterIntroPosition;
	public Vector3 cameraAfterIntroRotation;
	public bool introEnabled = true;
	
	static bool m_IsIntroShown;

	Canvas m_Canvas;
	GameCamera m_Camera;
	Beetle m_Beetle;
	Image m_CanGrabIndicator;
	Text m_PopupMessage;
	GameObject m_Intro, m_Ending;
	Animator m_IntroAnimator, m_EndingAnimator;
	float m_PopupMessageTime;
	Vector3 m_CameraInitialPos, m_CameraInitialRot;
	float m_CameraIntroTransitionTime;

	void Start()
	{
		m_Canvas = GetComponent <Canvas> ();
		m_Camera = camera.GetComponent <GameCamera> ();
		m_CameraInitialPos = m_Camera.transform.position;
		m_CameraInitialRot = m_Camera.transform.eulerAngles;
		m_Beetle = beetle.GetComponent <Beetle> ();
		m_CanGrabIndicator = transform.Find("CanGrabIndicator").GetComponent <Image> ();
		m_PopupMessage = transform.Find("PopupMessage").GetComponent <Text> ();
		m_Intro = transform.Find("Intro").gameObject;
		m_IntroAnimator = m_Intro.GetComponent <Animator> ();
		m_Ending = transform.Find("Ending").gameObject;
		m_EndingAnimator = m_Ending.GetComponent <Animator> ();

		if (!introEnabled || m_IsIntroShown)
		{
			m_Canvas.renderMode = RenderMode.ScreenSpaceOverlay;
			m_Camera.enabled = true;
			m_Camera.SetEnabled(true);
			m_Beetle.SetCanControl(true);
		}
		else
		{
			m_Intro.SetActive(true);
		}
	}

	void Update()
	{
		if (m_PopupMessageTime > 0)
		{
			m_PopupMessageTime -= Time.deltaTime;
			if (m_PopupMessageTime <= 0)
			{
				m_PopupMessage.enabled = false;
			}
		}

		if (m_CameraIntroTransitionTime > 0)
		{
			m_CameraIntroTransitionTime -= Time.deltaTime;
			float t = m_CameraIntroTransitionTime / cameraIntroTransitionTime;
			m_Camera.transform.position = Vector3.Lerp(cameraAfterIntroPosition, m_CameraInitialPos, t);
			m_Camera.transform.rotation = Quaternion.Euler(Vector3.Lerp(cameraAfterIntroRotation, m_CameraInitialRot, t));
			if (m_CameraIntroTransitionTime <= 0)
			{
				m_Camera.enabled = true;
				m_Camera.SetEnabled(true);
				m_Beetle.SetCanControl(true);
			}
		}

		if (m_Intro.activeSelf && m_IntroAnimator.GetCurrentAnimatorStateInfo(0).IsName("End"))
		{
			m_Intro.SetActive(false);
			m_Canvas.renderMode = RenderMode.ScreenSpaceOverlay;
			m_CameraIntroTransitionTime = cameraIntroTransitionTime;
			m_IsIntroShown = true;
		}
		if (m_Ending.activeSelf && Input.anyKeyDown && m_EndingAnimator.GetCurrentAnimatorStateInfo(0).IsName("End"))
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}
	}

	public void SetCanGrabIndicatorVisible(bool visible)
	{
		m_CanGrabIndicator.enabled = visible;
	}

	public void ShowMessage(string message, float delay = 0)
	{
		if (delay == 0) delay = messageDefaultDelay;
		m_PopupMessageTime = messageDefaultDelay;
		m_PopupMessage.text = message;
		m_PopupMessage.enabled = true;
	}

	public void RunIntro()
	{
		m_Intro.SetActive(true);
	}

	public void RunEnding()
	{
		m_IsIntroShown = false; // to restart intro if we fully passed the game
		m_Camera.SetEnabled(false);
		m_Ending.SetActive(true);
	}
}
