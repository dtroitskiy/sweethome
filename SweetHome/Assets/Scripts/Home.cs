﻿using UnityEngine;

public class Home : MonoBehaviour
{
	public Beetle beetle;
	public float reduceByFrictionFactor = 0.01f;
	public float minumumSize = 0.2f;
	public float maximumSize = 1.6f;
	public float buildFactor = 0.2f;
	public float buildMassDivisor = 0.2f;
	public float buildMassPower = 3.0f;
	public float buildSpeed = 1.0f;
	public float pushForceFactor = 3.0f;
	public float pushForceStuckFactor = 500.0f;
	public float minVelocityToDetectRoll = 0.1f;
	public float minVelocityToDetectStuck = 0.2f;
	
	Rigidbody m_Rigidbody;
	AudioSource m_RollSound, m_HideSound, m_BuildSound;
	Vector3 m_OldPosition;
	float m_BuildAmount;
	bool m_PushForceApplied;
	float m_StuckForceFactor;
	bool m_Grabbed;
	
	void Start()
	{
		m_Rigidbody = GetComponent <Rigidbody> ();
		m_RollSound = GetComponents <AudioSource> ()[0];
		m_HideSound = GetComponents <AudioSource> ()[1];
		m_BuildSound = GetComponents <AudioSource> ()[2];
		m_OldPosition = transform.position;
	}

	void FixedUpdate()
	{
		if (m_BuildAmount > 0)
		{
			float buildAmount = Time.deltaTime * buildSpeed;
			float diff = m_BuildAmount - buildAmount;
			if (diff < 0)
			{
				buildAmount = m_BuildAmount;
				m_BuildAmount = 0;
			}
			else
			{
				m_BuildAmount = diff;
			}
			var newScale = transform.localScale.x + buildAmount;
			if (newScale > maximumSize) newScale = maximumSize;
			transform.localScale = new Vector3(newScale, newScale, newScale);
		}
		if (transform.localScale.x > minumumSize && m_OldPosition != transform.position)
		{
			float scaleDelta = Vector3.Distance(m_OldPosition, transform.position) * reduceByFrictionFactor;
			transform.localScale -= new Vector3(scaleDelta, scaleDelta, scaleDelta);
			UpdateMass();
		}
		m_OldPosition = transform.position;
		if (m_Grabbed)
		{
			beetle.RotateToHome();
		}
		if (m_Rigidbody.velocity.magnitude > minVelocityToDetectRoll)
		{
			m_StuckForceFactor = 0;
			if (!m_RollSound.isPlaying) m_RollSound.Play();
		}
		else
		{
			if (m_RollSound.isPlaying) m_RollSound.Pause();
		}
		if (m_Grabbed && m_PushForceApplied)
		{
			if (m_Rigidbody.velocity.magnitude < minVelocityToDetectStuck)
			{
				m_StuckForceFactor += Time.deltaTime * pushForceStuckFactor;
			}
			else
			{
				m_StuckForceFactor -= Time.deltaTime * pushForceStuckFactor;
				if (m_StuckForceFactor < 0) m_StuckForceFactor = 0;
			}
		}
		else
		{
			m_StuckForceFactor = 0;
			m_PushForceApplied = false;
		}
	}

	public void Push(Vector3 move)
	{
		var force = move * m_Rigidbody.mass * (pushForceFactor + m_StuckForceFactor);
		if (force.sqrMagnitude > 0) m_PushForceApplied = true;
		m_Rigidbody.AddForce(force, ForceMode.Force);
	}

	public bool IsGrabbed()
	{
		return m_Grabbed;
	}

	public void Grab()
	{
		m_Grabbed = true;
	}

	public void Release()
	{
		m_Grabbed = false;
	}

	public void Build()
	{
		m_BuildAmount += buildFactor;
		m_BuildSound.Play();
	}

	public void Consume(Shit shit)
	{
		Build();
		Destroy(shit.gameObject);
	}

	void UpdateMass()
	{
		m_Rigidbody.mass = Mathf.Pow(transform.localScale.x / buildMassDivisor, buildMassPower);
	}

	public void PlayHideSound()
	{
		m_HideSound.Play();
	}
}
