using UnityEngine;
using UnityEngine.SceneManagement;

public class Beetle : MonoBehaviour
{
	public GameObject ui;
	public Home home;
	public GameObject bird;
	public Fox fox;
	public GameObject winPoint;

	public float moveSpeedFactor = 1.5f;
	public float movingTurnSpeed = 360;
	public float stationaryTurnSpeed = 180;
	public float animSpeedMultiplier = 1.5f;
	public float animSpeedClampFactor = 1.5f;
	public float animSpeedClampMax = 2.0f;
	public float minForwardAmountToCrawl = 0.05f;
	public float groundCheckDistance = 0.1f;
	public float ballInteractDistanceFactor = 1.5f;
	public float ballMaxSpeedToReleaseGrab = 5.0f;
	public float grabTransitionTime = 1.6f;
	public float grabJointLimitFactor = 0.55f;
	public float grabOffset = 0.45f;
	public float dropDistance = 0.1f;
	public float minHomeSizeToGetInSide = 0.4f;
	public float getInsideSpeed = 0.5f;
	public float winRadius = 2.0f;
	public float drowningTime = 3.0f;
	public float drowningRotationTime = 1.0f;
	public float drowningAnimSpeed = 2.0f;
	public float drowningDrag = 20.0f;
	
	Transform m_BugPivot;
	Transform m_CarryPivot;
	UI m_UI;
	Rigidbody m_Rigidbody, m_HomeRigidbody;
	CapsuleCollider m_Capsule;
	ConfigurableJoint m_Joint;
	Animator m_Animator;

	bool m_IsGrounded;
	float m_TurnAmount;
	float m_ForwardAmount;
	Vector3 m_GroundNormal;
	bool m_CanControl;
	Shit m_CarryingShit = null;
	bool m_Grab = false;
	float m_GrabTransitionCurTime;
	bool m_InsideHome = false, m_JustGotInsideHome = false;
	Vector3 m_DirectionToHome;
	float m_DrowningTime, m_DrowningRotationTime;
	
	void Start()
	{
		m_BugPivot = transform.Find("BugPivot");
		m_CarryPivot = m_BugPivot.transform.Find("CarryPivot");
		m_UI = ui.GetComponent <UI> ();
		m_Rigidbody = GetComponent <Rigidbody> ();
		m_HomeRigidbody = home.GetComponent <Rigidbody> ();
		m_Capsule = GetComponent <CapsuleCollider> ();
		m_Joint = GetComponent <ConfigurableJoint> ();
		m_Animator = GetComponentInChildren <Animator> ();
		
		m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
	}

	void Update()
	{
		if (m_DrowningTime > 0)
		{
			if (m_DrowningRotationTime > 0)
			{
				m_DrowningRotationTime -= Time.deltaTime;
				var rot = transform.localRotation.eulerAngles;
				transform.localRotation = Quaternion.Euler(rot.x, rot.y, Mathf.Lerp(180, 0, m_DrowningRotationTime / drowningRotationTime));
			}
			else
			{
				m_UI.ShowMessage("You have drowned");
			}
			m_DrowningTime -= Time.deltaTime;
			if (m_DrowningTime <= 0)
			{
				SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
			}
		}
		else
		{
			Vector3 beetlePos = transform.position;
			Vector3 homePos = home.transform.position;
			float delta = (beetlePos - homePos).magnitude - home.transform.localScale.x;
		
			if (delta < dropDistance && m_CarryingShit != null)
			{
				BuildHome();
			}

			bool canGrab = CanGrab();
			m_UI.SetCanGrabIndicatorVisible(canGrab);
			if (canGrab || m_InsideHome)
			{
				SoftJointLimit lim = new SoftJointLimit();
				lim.limit = m_Capsule.radius * transform.localScale.x + home.transform.localScale.x * grabJointLimitFactor;
				m_Joint.linearLimit = lim;
			}

			if (m_Grab)
			{
				if (m_HomeRigidbody.velocity.magnitude >= ballMaxSpeedToReleaseGrab)
				{
					Release();
				}
				if (m_GrabTransitionCurTime > 0)
				{
					m_GrabTransitionCurTime -= Time.deltaTime;
					if (m_GrabTransitionCurTime < 0f) m_GrabTransitionCurTime = 0f;
					float offset = 1.0f - m_GrabTransitionCurTime / grabTransitionTime;
					m_BugPivot.transform.localPosition = Vector3.forward * offset * grabOffset;
					RotateToHome();
				}
			}

			if (m_InsideHome)
			{
				bool isHiding = m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Hide");
				bool isHidden = m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Hidden");
				if (isHiding)
				{
					RotateToHome();
					transform.Translate(transform.InverseTransformDirection(transform.forward) * getInsideSpeed * Time.deltaTime);
				}
				else if (isHidden)
				{
					transform.position = home.transform.position;

					if (!m_JustGotInsideHome)
					{
						m_JustGotInsideHome = true;

						Component[] renderers = gameObject.GetComponentsInChildren(typeof(Renderer));
						foreach (Renderer renderer in renderers)
						{
							renderer.enabled = false;
						}
					}
				}
			}

			CheckWin();
		}
	}

	public bool CanControl()
	{
		return m_CanControl;
	}

	public void SetCanControl(bool canControl)
	{
		m_CanControl = canControl;
	}

	public void Move(Vector3 move)
	{
		if (move.magnitude > 1.0f) move.Normalize();
		Vector3 localMove = transform.InverseTransformDirection(move);
		m_TurnAmount = Mathf.Atan2(localMove.x, localMove.z);
		m_ForwardAmount = localMove.z;
		ApplyExtraTurnRotation();
		
		Vector3 newVelocity = move * moveSpeedFactor;
		newVelocity.y = m_Rigidbody.velocity.y;
		m_Rigidbody.velocity = newVelocity;
		
		CheckGroundStatus();
		
		// rotating bug pivot towards ground normal angle
		if (!m_Grab)
		{
			float yAngle = transform.rotation.eulerAngles.y;
			var rotatedForward = Quaternion.AngleAxis(yAngle, m_GroundNormal) * Vector3.forward;
			var q = Quaternion.LookRotation(rotatedForward, m_GroundNormal);
			q = Quaternion.Euler(q.eulerAngles.x, 0, q.eulerAngles.z);
			var bugPivotRotation = m_BugPivot.transform.localRotation;
			m_BugPivot.transform.localRotation = Quaternion.Lerp(bugPivotRotation, q, Time.deltaTime);
		}

		// send input and other state parameters to the animator
		UpdateAnimator(localMove);
	}

	public void MoveHome(Vector3 move)
	{
		if (move.magnitude > 1f) move.Normalize();
		if (m_Grab && m_GrabTransitionCurTime == 0)
		{
			if (!home.IsGrabbed()) home.Grab();
			home.Push(move);
		}
	}

	public void RotateToHome()
	{
		Vector3 directionToHome = (home.transform.position - transform.position).normalized;
		directionToHome = transform.InverseTransformDirection(directionToHome);
		directionToHome.y = 0;
		float angle = Vector3.SignedAngle(Vector3.forward, directionToHome, Vector3.up) * Mathf.Deg2Rad;

		m_TurnAmount = angle;
		m_ForwardAmount = transform.InverseTransformDirection(home.GetComponent <Rigidbody> ().velocity).z;
		ApplyExtraTurnRotation();
		
		// send input and other state parameters to the animator
		UpdateAnimator(directionToHome);
	}

	public void TryGrabAPieceOfShit(Shit shit)
	{ 
		if (m_CarryingShit != null || m_Grab)
		{
			return;
		}
		shit.transform.parent = m_CarryPivot.transform;
		shit.transform.localPosition = new Vector3(0, 0, 0);
		m_CarryingShit = shit;
		shit.SetCarried(true);
	}

	void BuildHome()
	{
		Destroy(m_CarryingShit.gameObject);
		m_CarryingShit = null;
		home.Build();
	}

	public bool CanGrab()
	{
		Vector3 homePos = home.transform.position;
		float delta = (transform.position - home.transform.position).magnitude;
		var minDistance = (m_Capsule.radius * transform.localScale.x + home.transform.localScale.x * 0.5f) * ballInteractDistanceFactor;
		return delta < minDistance;
	}

	public void Grab()
	{
		m_GrabTransitionCurTime = grabTransitionTime;
		m_Animator.SetBool("isGrabbing", true);
		m_Grab = true;
		m_Rigidbody.mass = 0;
		m_Rigidbody.AddForce(-m_Rigidbody.velocity, ForceMode.VelocityChange);
		m_Joint.xMotion = ConfigurableJointMotion.Limited;
		m_Joint.yMotion = ConfigurableJointMotion.Limited;
		m_Joint.zMotion = ConfigurableJointMotion.Limited;
		m_Joint.enableCollision = false;
	}

	public void Release()
	{
		m_Animator.SetBool("isGrabbing", false);
		m_BugPivot.transform.localPosition = Vector3.zero;
		m_Grab = false;
		home.Release();
		m_Rigidbody.mass = 1.0f;
		m_Joint.xMotion = ConfigurableJointMotion.Free;
		m_Joint.yMotion = ConfigurableJointMotion.Free;
		m_Joint.zMotion = ConfigurableJointMotion.Free;
		m_Joint.enableCollision = true;
	}

	public bool HasGrab()
	{
		return m_Grab;
	}

	bool HomeIsBigEnough()
	{
		return home.transform.localScale.x > minHomeSizeToGetInSide;
	}

	private bool CanGetInsideHome()
	{
		return CanGrab() && HomeIsBigEnough();
	}

	public void ToggleGetInsideHome()
	{
		if (m_InsideHome)
		{
			bool isHiding = m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Hide");
			if (!isHiding)
			{
				GetOutFromHome();
			}
		}
		else
		{
			if (CanGetInsideHome())
			{
				GetInsideHome();
			}
			else if (CanGrab() && !HomeIsBigEnough())
			{
				m_UI.ShowMessage("Your ball is too small");
			}
		}
	}

	private void GetInsideHome()
	{
		m_InsideHome = true;
		m_Capsule.enabled = false;
		m_Rigidbody.useGravity = false;
		m_Rigidbody.mass = 0;
		m_Rigidbody.AddForce(-m_Rigidbody.velocity, ForceMode.VelocityChange);
		m_Joint.xMotion = ConfigurableJointMotion.Limited;
		m_Joint.yMotion = ConfigurableJointMotion.Limited;
		m_Joint.zMotion = ConfigurableJointMotion.Limited;
		m_Joint.enableCollision = false;
		m_Animator.SetTrigger("Hide");
		home.PlayHideSound();
	}

	private void GetOutFromHome()
	{
		m_InsideHome = m_JustGotInsideHome = false;
		m_Capsule.enabled = true;
		m_Rigidbody.useGravity = true;
		m_Rigidbody.mass = 1.0f;
		m_Joint.xMotion = ConfigurableJointMotion.Free;
		m_Joint.yMotion = ConfigurableJointMotion.Free;
		m_Joint.zMotion = ConfigurableJointMotion.Free;
		m_Joint.enableCollision = true;
		m_Animator.SetTrigger("Unhide");
		home.PlayHideSound();
		Component[] renderers = gameObject.GetComponentsInChildren(typeof(Renderer));
		foreach (Renderer renderer in renderers)
		{
			renderer.enabled = true;
		}
	}

	void UpdateAnimator(Vector3 move)
	{
		m_Animator.SetBool("isCrawling", m_ForwardAmount >= minForwardAmountToCrawl);
		m_Animator.SetFloat("turn", Mathf.Clamp(0.5f + m_TurnAmount, 0, 1));
		float speedClamped = Mathf.Clamp(m_Rigidbody.velocity.magnitude * animSpeedClampFactor, 0, animSpeedClampMax);
		m_Animator.SetFloat("speed", animSpeedMultiplier * speedClamped);
	}

	void ApplyExtraTurnRotation()
	{
		// help the character turn faster (this is in addition to root rotation in the animation)
		float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, m_ForwardAmount);
		transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
	}

	void CheckGroundStatus()
	{
		RaycastHit hitInfo;
		var rayStart = transform.position;
#if UNITY_EDITOR
		// helper to visualise the ground check ray in the scene view
		Debug.DrawLine(rayStart, rayStart + (Vector3.down * groundCheckDistance));
#endif
		// 0.1f is a small offset to start the ray from inside the character
		// it is also good to note that the transform position in the sample assets is at the base of the character
		if (Physics.Raycast(rayStart, Vector3.down, out hitInfo, groundCheckDistance))
		{
			m_GroundNormal = hitInfo.normal;
			m_IsGrounded = true;
		}
		else
		{
			m_IsGrounded = false;
			m_GroundNormal = Vector3.up;
		}
	}

	public bool IsInsideHome()
	{
		return m_InsideHome;
	}

	void CheckWin()
	{
		if (m_CanControl && (transform.position - winPoint.transform.position).magnitude < winRadius)
		{
			m_Animator.enabled = false;
			m_CanControl = false;
			bird.SetActive(false);
			m_UI.RunEnding();
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.name == "FoxTrigger1" || other.name == "FoxTrigger2")
		{
			if (fox) fox.ActivateFox();
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		if (LayerMask.LayerToName(collision.gameObject.layer) == "Water")
		{
			m_CanControl = false;
			bird.SetActive(false);
			m_DrowningTime = drowningTime;
			m_DrowningRotationTime = drowningRotationTime;
			collision.gameObject.GetComponent <MeshCollider> ().enabled = false;
			m_Rigidbody.drag = drowningDrag;
			m_Animator.SetBool("isCrawling", true);
			m_Animator.SetFloat("speed", drowningAnimSpeed);
		}
	}
}
