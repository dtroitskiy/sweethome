﻿using UnityEngine;

public class Fox : MonoBehaviour
{
	public float speed = 10.0f;
	public float runForce = 20.0f;
	public float activeTime = 15.0f;
	
	Rigidbody m_RigidBody;
	bool m_Active;
	float m_CurTime;
	
	private void Start()
	{
		m_RigidBody = GetComponent <Rigidbody> ();
	}

	void FixedUpdate()
	{
		if (m_Active)
		{
			m_RigidBody.AddForce(transform.forward * runForce, ForceMode.Force);
		}
	}

	private void Update()
	{
		if (m_Active)
		{
			m_CurTime -= Time.deltaTime;
		}
		if (m_CurTime < 0)
		{
			Destroy(gameObject);
		}
	}

	public void ActivateFox()
	{
		GetComponentInChildren <SkinnedMeshRenderer> ().enabled = true;
		m_CurTime = activeTime;
		m_Active = true;
	}
}
