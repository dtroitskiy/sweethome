using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class BeetleControls : MonoBehaviour
{
	private Beetle m_Beetle;
	private Transform m_Cam;
	private Vector3 m_CamForward;
	private Vector3 m_Move;
	private bool m_Grab = false;					  
	
	private void Start()
	{
		if (Camera.main != null)
		{
			m_Cam = Camera.main.transform;
		}
		m_Beetle = GetComponent <Beetle> ();
	}

	private void Update()
	{
		if (CrossPlatformInputManager.GetButtonUp("GoInside"))
		{
			m_Beetle.ToggleGetInsideHome();
		}
		bool grab = CrossPlatformInputManager.GetButton("Grab");
		if (!grab) grab = CrossPlatformInputManager.GetAxis("Grab") > 0;
		if (grab && !m_Grab && m_Beetle.CanGrab())
		{
			m_Beetle.Grab();
			m_Grab = grab;
		}
		if (!grab && m_Grab)
		{ 
			m_Beetle.Release();
			m_Grab = grab;
		}
	}

	private void FixedUpdate()
	{
		if (!m_Beetle.CanControl() || m_Beetle.IsInsideHome()) return;
		
		float x = CrossPlatformInputManager.GetAxis("MoveX");
		float y = CrossPlatformInputManager.GetAxis("MoveY");
		m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
		m_Move = y * m_CamForward + x * m_Cam.right;
		if (m_Grab && m_Beetle.HasGrab())
		{
			m_Beetle.MoveHome(m_Move);
		}
		else
		{
			m_Beetle.Move(m_Move);
		}
	}
}
