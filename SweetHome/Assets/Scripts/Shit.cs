﻿using UnityEngine;

public class Shit : MonoBehaviour
{
	public float grabDistance = 0.3f;
	public bool autoConsumedByBall = false;

	Beetle m_Beetle;
	Home m_Home;
	bool m_IsCarried;

	void Start()
	{
		m_Beetle = FindObjectOfType <Beetle> ();
		m_Home = FindObjectOfType <Home> ();
	}

	void Update()
	{
		// beetle grabs shit
		float distance = Vector3.Distance(m_Beetle.transform.position, transform.position);
		if (distance < grabDistance)
		{
			m_Beetle.TryGrabAPieceOfShit(this);
		}
		// home consumes shit
		if (autoConsumedByBall && !m_IsCarried)
		{
			distance = Vector3.Distance(m_Home.transform.position, transform.position);
			if (distance < m_Home.transform.localScale.x)
			{
				m_Home.Consume(this);
			}
		}
	}

	public void SetCarried(bool carried)
	{
		m_IsCarried = carried;
	}
}
