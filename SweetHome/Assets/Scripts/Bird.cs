﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Bird : MonoBehaviour
{
	[SerializeField] float timeToNextHuntMin = 20, timeToNextHuntMax = 40;
	[SerializeField] float flySpeed = 30;
	[SerializeField] float flyHeight = 10;
	[SerializeField] int diveCount = 3;
	[SerializeField] float shadowFlyTime = 0.4f;
	[SerializeField] float shadowFlyDistance = 40.0f;
	[SerializeField] ulong crySoundDelay = 20000;
	[SerializeField] float deathTime = 3.0f;
	[SerializeField] Beetle beetle;
	[SerializeField] GameObject shadow;
	[SerializeField] GameObject ui;

	SkinnedMeshRenderer m_SMR;
	GameObject m_Shadow;
	GameCamera m_Camera;
	UI m_UI;
	AudioSource m_AudioSourceCry, m_AudioSourceWing, m_AudioSourceBeetleDeath, m_AlertMusic;
	
	float m_TimeToNextHunt;
	bool m_IsDiving, m_IsEating;
	int m_DiveCount;
	Vector3 m_DiveVector;
	float m_ShadowTime, m_HalfShadowFlyTime, m_DeathTime;
	
	void Start()
	{
		m_SMR = GetComponentInChildren <SkinnedMeshRenderer> ();
		m_SMR.enabled = false;
		m_Camera = Camera.main.GetComponent <GameCamera> ();
		m_UI = ui.GetComponent <UI> ();
		var audioSources = GetComponents <AudioSource> ();
		m_AudioSourceCry = audioSources[0];
		m_AudioSourceWing = audioSources[1];
		m_AudioSourceBeetleDeath = audioSources[2];
		m_AlertMusic = audioSources[3];

		PlanHunt();
	}

	void Update()
	{
		if (m_DeathTime > 0)
		{
			m_DeathTime -= Time.deltaTime;
			if (m_DeathTime <= 0)
			{
				SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
			}
		}
		else
		{
			if (m_IsEating)
			{
				m_ShadowTime -= Time.deltaTime;
				m_Shadow.transform.localPosition = Vector3.forward + Vector3.left * (m_ShadowTime - m_HalfShadowFlyTime) * shadowFlyDistance;
				if (m_ShadowTime >= m_HalfShadowFlyTime && m_ShadowTime - Time.deltaTime < m_HalfShadowFlyTime)
				{
					beetle.gameObject.SetActive(false);
					m_AudioSourceBeetleDeath.Play();
				}
				if (m_ShadowTime <= 0)
				{
					m_UI.ShowMessage("You have been eaten");
					m_DeathTime = deathTime;
				}
			}
			else
			{
				if (m_TimeToNextHunt > 0)
				{
					m_TimeToNextHunt -= Time.deltaTime;
					if (m_TimeToNextHunt <= 0)
					{
						m_DiveCount = diveCount;
						m_AlertMusic.Play();
						StartDive();
					}
				}
				else
				{
					if (m_IsDiving)
					{
						UpdateDive();
					}
					else
					{
						if (m_DiveCount > 0)
						{
							StartDive();
						}
						else
						{
							m_AlertMusic.Stop();
							if (!beetle.IsInsideHome())
							{
								EatBeetle();
							}
							PlanHunt();
						}
					}
				}
			}
		}
		if (m_AudioSourceCry.time > 0 && !m_Camera.IsVignetteActive())
		{
			m_Camera.ShowVignette();
		}
	}

	void PlanHunt()
	{
		m_TimeToNextHunt = Random.Range(timeToNextHuntMin, timeToNextHuntMax);
	}

	void StartDive()
	{
		m_SMR.enabled = true;
		m_IsDiving = true;
		var randomDir = Random.insideUnitCircle.normalized;
		m_DiveVector = new Vector3(randomDir.x, 0, randomDir.y);
		transform.position = beetle.transform.position + Vector3.up * flyHeight - m_DiveVector * flySpeed;
		transform.LookAt(transform.position + m_DiveVector);
		m_AudioSourceCry.Play(crySoundDelay);
	}

	void UpdateDive()
	{
		transform.position += m_DiveVector * flySpeed * Time.deltaTime;
		var noHeightPos = transform.position - Vector3.up * flyHeight;
		if (Vector3.Distance(noHeightPos, beetle.transform.position) >= flySpeed * 2)
		{
			EndDive();
		}
	}

	void EndDive()
	{
		m_SMR.enabled = false;
		m_IsDiving = false;
		--m_DiveCount;
	}

	void EatBeetle()
	{
		m_IsEating = true;
		m_Shadow = Instantiate(shadow, Camera.main.transform);
		m_ShadowTime = shadowFlyTime;
		m_HalfShadowFlyTime = m_ShadowTime / 2;
		m_AudioSourceWing.Play();
	}
}
