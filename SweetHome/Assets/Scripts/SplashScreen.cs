﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour
{
	public float inputDelayTime = 1.0f;
	public float autoStartTime = 10.0f;

	float m_InputDelayTime, m_AutoStartTime;

	void Start()
	{
		m_InputDelayTime = inputDelayTime;
		m_AutoStartTime = autoStartTime;
	}

	void Update()
	{
		m_InputDelayTime -= Time.deltaTime;
		m_AutoStartTime -= Time.deltaTime;

		if ((m_InputDelayTime < 0 && Input.anyKeyDown) || m_AutoStartTime < 0)
		{
			SceneManager.LoadScene("Game");
		}
	}
}
