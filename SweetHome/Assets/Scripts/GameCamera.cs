﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class GameCamera : MonoBehaviour
{
	[SerializeField] GameObject music;
	[SerializeField] Transform beetle;
	[SerializeField] GameObject fireflies;
	[SerializeField] Vector3 baseOffset;
	[SerializeField] Vector2 rotationSpeed;
	[SerializeField] Vector2 startRotation;
	[SerializeField] float minAngleY;
	[SerializeField] float maxAngleY;
	[SerializeField] float vignetteStartIntensity;
	[SerializeField] float vignetteEndIntensity;
	[SerializeField] float vignetteFadeTime;

	PostProcessVolume m_PPV;
	bool m_Enabled;
	Vector3 m_Offset;
	Vector2 m_Rotation;
	DepthOfField m_DoF;
	Vignette m_Vignette;
	float m_VignetteTime;

	void Awake()
	{
		var existingMusic = GameObject.FindGameObjectWithTag("Music");
		if (!existingMusic)
		{
			var m = Instantiate(music);
			DontDestroyOnLoad(m);
		}
	}

	void Start()
	{
		Cursor.visible = false;

		m_PPV = GetComponent <PostProcessVolume> ();
		m_DoF = m_PPV.profile.GetSetting <DepthOfField> ();
		m_DoF.active = true;
		m_Vignette = m_PPV.profile.GetSetting <Vignette> ();

		Instantiate(fireflies, transform);
		
		m_Rotation = startRotation;
		ApplyRotation();
	}

	void Update()
	{
		if (!m_Enabled) return;

		ApplyRotation();

		if (m_VignetteTime > 0)
		{
			m_VignetteTime -= Time.deltaTime;
			if (m_VignetteTime < 0)
			{
				m_VignetteTime = 0;
				m_Vignette.active = false;
			}
			else
			{
				var v = Mathf.Lerp(vignetteEndIntensity, vignetteStartIntensity, m_VignetteTime / vignetteFadeTime);
				m_Vignette.intensity.value = v;
			}
		}

		if (Input.GetButtonDown("Cancel"))
		{
			Application.Quit();
		}
	}

	void LateUpdate()
	{
		if (!m_Enabled) return;

		m_Rotation.y = Mathf.Clamp(m_Rotation.y + Input.GetAxis("LookY") * rotationSpeed.y, minAngleY, maxAngleY);
		m_Rotation.x += Input.GetAxis("LookX") * rotationSpeed.x;
		RaycastHit hit;
		if (Physics.Raycast(beetle.position, transform.TransformDirection(Vector3.back), out hit, 2f))
		{
			if (hit.collider.gameObject.name!="Home")
			{
				transform.position = hit.point + Vector3.up * 0.04f;
			}
		}
	}

	void ApplyRotation()
	{
		Quaternion camRotationH = Quaternion.AngleAxis(m_Rotation.x, Vector3.up);
		Quaternion camRotationV = Quaternion.AngleAxis(m_Rotation.y, Vector3.right);
		m_Offset = camRotationH * camRotationV * baseOffset;
		transform.position = beetle.transform.position + m_Offset;
		transform.rotation = camRotationH * camRotationV;
	}

	public void SetEnabled(bool enabled)
	{
		m_Enabled = enabled;
	}

	public void SetDoFEnabled(bool enabled)
	{
		if (!m_Enabled) return;
		m_DoF.active = enabled;
	}

	public void ShowVignette()
	{
		if (!m_Enabled) return;

		m_Vignette.active = true;
		m_Vignette.intensity.value = vignetteStartIntensity;
		m_VignetteTime = vignetteFadeTime;
	}

	public bool IsVignetteActive()
	{
		return m_Vignette.active;
	}
}
