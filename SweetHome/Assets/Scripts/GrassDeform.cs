﻿using UnityEngine;
[ExecuteInEditMode]
public class GrassDeform : MonoBehaviour
{
	public Transform beetle;
	public Transform home;

	Material material;

	private void Start()
	{
		material = GetComponent <MeshRenderer> ().sharedMaterial;
	}

	private void LateUpdate()
	{
		if (beetle != null) material.SetVector("Player_Position", beetle.position);
		if (home != null) material.SetVector("Home_Position", home.position);
	}
}
